SUMMARY = "C library for ptags"
DESCRIPTION = "User land library for manipulation of process tags (LSM ptags)"
HOMEPAGE = "https://gitlab.com/jobol/ptags"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d17fe8b881c82af0f56b26673f65354e"

SRC_URI = "git://gitlab.com/jobol/ptags.git;protocol=https"
SRCREV = "e901f9c320fb7c988fadd477ddd41e31bc4cb175"
S = "${WORKDIR}/git"

inherit cmake

PACKAGES =+ "${PN}-tools"
FILES_${PN}-tools = "${bindir}/ptags-*"

