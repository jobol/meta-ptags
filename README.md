This repository contains yocto/oe compatible meta
for integrating the ptags LSM in the kernel 4.4
of Krogoth

This repository:
  https://gitlab.com/jobol/meta-ptags

User-land library and sources:
  https://gitlab.com/jobol/ptags

How to use it? Just add this directory
in your file bblayers.conf. This will add the ptags
feature to your kernel.

For using the userland library libptags, add
a DEPENDS += "libptags" in your recipes.

